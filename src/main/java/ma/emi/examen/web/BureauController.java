package ma.emi.examen.web;

import ma.emi.examen.entities.Bureau;
import ma.emi.examen.repositories.BureauRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@RestController
@RequestMapping("/bureau")

public class BureauController {
    private BureauRepository bureauRepository;

    public BureauController(BureauRepository bureauRepository) {
        this.bureauRepository = bureauRepository;
    }

    @GetMapping("/")
    public List<Bureau> getAll(){
        return bureauRepository.findAll();
    }

    @GetMapping("/{id}")
    public Bureau getBureau(@PathVariable Long id){
        return bureauRepository.findById(id).get();
    }

    @PostMapping("/add")
    public Bureau addBureau(@RequestBody Bureau bureau){
        return bureauRepository.save(bureau);
    }

    @DeleteMapping("/delete/{id}")
    public void supprimerBureau(@PathVariable Long id){
        bureauRepository.deleteById(id);
    }


}
