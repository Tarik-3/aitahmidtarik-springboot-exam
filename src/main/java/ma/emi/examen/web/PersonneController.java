package ma.emi.examen.web;

import ma.emi.examen.entities.Employie;
import ma.emi.examen.entities.Personne;
import ma.emi.examen.entities.Prof;
import ma.emi.examen.entities.Salle;
import ma.emi.examen.repositories.EmployieRepository;
import ma.emi.examen.repositories.PersonneRepository;
import ma.emi.examen.repositories.ProfRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/personne")
public class PersonneController {
    private PersonneRepository personneRepository;
    private ProfRepository profRepository;
    private EmployieRepository employieRepository;

    public PersonneController(PersonneRepository personneRepository, ProfRepository profRepository, EmployieRepository employieRepository) {
        this.personneRepository = personneRepository;
        this.profRepository = profRepository;
        this.employieRepository = employieRepository;
    }

    @GetMapping("/")
    public List<Personne> getAll(){
        return personneRepository.findAll();
    }

    @GetMapping("/{id}")
    public Personne getPersonne(@PathVariable Long id){
        return personneRepository.findById(id).get();
    }

    @PostMapping("/add")
    public Personne addPersonne(@RequestBody Personne personne){
        return personneRepository.save(personne);
    }
    @PutMapping("/modifier/{id}")
    public void updatePersone(@PathVariable Long id, @RequestBody Personne personne){
        personneRepository.findById(id).map(r->{
            r.setNom(personne.getNom());

            return personneRepository.save(r);
        });
    }
    @PutMapping("/prof/{id}")
    public void updateProf(@PathVariable Long id, @RequestBody Prof prof){
        profRepository.findById(id).map(r->{
            r.setNom(prof.getNom());
            r.setDepartement(prof.getDepartement());
            return personneRepository.save(r);
        });
    }
    @PutMapping("/employer/{id}")
    public void updateEmployer(@PathVariable Long id, @RequestBody Employie employie){
        employieRepository.findById(id).map(r->{
            r.setNom(employie.getNom());
            r.setBureau(employie.getBureau());
            return personneRepository.save(r);
        });
    }

    @DeleteMapping("/delete/{id}")
    public void supprimerPersonne(@PathVariable Long id){
        personneRepository.deleteById(id);
    }


}
