package ma.emi.examen.web;

import ma.emi.examen.entities.Salle;
import ma.emi.examen.repositories.SalleRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/salle")
public class SalleController {
    private SalleRepository salleRepository;

    public SalleController(SalleRepository salleRepository) {
        this.salleRepository = salleRepository;
    }
    @GetMapping("/")
    public List<Salle> getAll(){
        return salleRepository.findAll();
    }

    @GetMapping("/get/{id}")
    public Salle getSalle(@PathVariable Long id){
        return salleRepository.findById(id).get();
    }

    @PostMapping("/add")
    public Salle addSalle(@RequestBody Salle salle){
        return salleRepository.save(salle);
    }
    @PutMapping("/{id}")
    public void updateSalle(@PathVariable Long id, @RequestBody Salle salle){
        salleRepository.findById(id).map(r->{
            r.setUniversite(salle.getUniversite());
            r.setNgSiege(salle.getNgSiege());
            return salleRepository.save(r);
        });
    }

    @DeleteMapping("/delete/{id}")
    public void supprimerSalle(@PathVariable Long id){
        salleRepository.deleteById(id);
    }


}
