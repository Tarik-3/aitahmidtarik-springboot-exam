package ma.emi.examen.web;

import ma.emi.examen.entities.Bureau;
import ma.emi.examen.entities.Departement;
import ma.emi.examen.repositories.DepartementRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/departement")
public class DepartementController {
    private DepartementRepository departementRepository;

    public DepartementController(DepartementRepository departementRepository) {
        this.departementRepository = departementRepository;
    }

    @GetMapping("/")
    public List<Departement> getAll(){
        return departementRepository.findAll();
    }

    @GetMapping("/{nom}")
    public Departement getDepartement(@PathVariable String nom){
        return departementRepository.findByNom(nom);
    }
    @GetMapping("/get/{id}")
    public Departement getDepartement(@PathVariable Long id){
        return departementRepository.findById(id).get();
    }

    @PostMapping("/add")
    public Departement addDepartement(@RequestBody Departement departement){
        return departementRepository.save(departement);
    }
    @PutMapping("/modifier/{id}")
    public void updateDepar(@PathVariable Long id, @RequestBody Departement departement){
        departementRepository.findById(id).map(r->{
            r.setNom(departement.getNom());
            r.setBureaus(departement.getBureaus());
            return departementRepository.save(r);
        });
    }

    @DeleteMapping("/delete/{nom}")
    public void supprimerDepartement(@PathVariable String nom){
        departementRepository.deleteByNom(nom);
    }
    @DeleteMapping("/delete/{id}")
    public void supprimerDepartement(@PathVariable Long id){
        departementRepository.deleteById(id);
    }




}
