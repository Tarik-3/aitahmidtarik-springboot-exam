package ma.emi.examen.web;

import ma.emi.examen.entities.Employie;
import ma.emi.examen.entities.Personne;
import ma.emi.examen.entities.Prof;
import ma.emi.examen.entities.Universite;
import ma.emi.examen.repositories.UniversiteRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/universite")
public class UniversiteController {

    private UniversiteRepository universiteRepository;


    public UniversiteController(UniversiteRepository universiteRepository) {
        this.universiteRepository = universiteRepository;
    }


    @GetMapping("/")
    public List<Universite> getAll(){
        return universiteRepository.findAll();
    }

    @GetMapping("/{id}")
    public Universite getUniversite(@PathVariable Long id){
        return universiteRepository.findById(id).get();
    }

    @PostMapping("/add")
    public Universite addUniversite(@RequestBody Universite universite){
        return universiteRepository.save(universite);
    }
    @PutMapping("/modifier/{id}")
    public void updateUniversiteUniversite(@PathVariable Long id, @RequestBody Universite universite){
        universiteRepository.findById(id).map(r->{
            r.setNom(universite.getNom());
            r.setBureaus(universite.getBureaus());
            r.setDepartements(universite.getDepartements());
            r.setSalles(universite.getSalles());
            r.setPersonneMap(universite.getPersonneMap());

            return universiteRepository.save(r);
        });
    }


    @DeleteMapping("/delete/{id}")
    public void supprimerUniversite(@PathVariable Long id){
        universiteRepository.deleteById(id);
    }


}
