package ma.emi.examen.repositories;

import ma.emi.examen.entities.Bureau;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BureauRepository extends JpaRepository<Bureau,Long> {
}
