package ma.emi.examen.repositories;

import ma.emi.examen.entities.Employie;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployieRepository extends JpaRepository<Employie,Long> {
}
