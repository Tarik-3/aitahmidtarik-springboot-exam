package ma.emi.examen.repositories;

import ma.emi.examen.entities.Prof;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfRepository extends JpaRepository<Prof,Long> {
}
