package ma.emi.examen.repositories;

import ma.emi.examen.entities.Departement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DepartementRepository extends JpaRepository<Departement,Long> {
    Departement findByNom(String nom);
    void deleteByNom(String nom);
}
