package ma.emi.examen.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;

@Entity
@Table(name = "EMPLOYER")
public class Employie extends Personne{
    @ManyToOne
    @JoinColumn(nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Bureau bureau;


    public Employie(String nom, Bureau bureau) {
        super(nom);
        this.bureau = bureau;
    }

    public Bureau getBureau() {
        return bureau;
    }

    public void setBureau(Bureau bureau) {
        this.bureau = bureau;
    }

    public Employie() {
    }


    public Employie creer(){
        return new Employie();
    }
    public void modifier(){}
    public void afficher(){}

//    public Bureau getBureau() {
//        return bureau;
//    }

//    public void setBureau(Bureau bureau) {
//        this.bureau = bureau;
//    }


    @Override
    public String toString() {
        return "Employie{" +
                super.toString() +
//                "bureau=" + bureau +
                '}';
    }
}
