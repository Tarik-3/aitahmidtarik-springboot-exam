package ma.emi.examen.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;

@Entity
public class Salle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int ngSiege;
    @ManyToOne
    @JoinColumn(nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Universite universite;

    public Salle(int ngSiege, Universite universite) {
        this.ngSiege = ngSiege;
        this.universite = universite;
    }

    public Universite getUniversite() {
        return universite;
    }

    public void setUniversite(Universite universite) {
        this.universite = universite;
    }

    public Salle(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNgSiege() {
        return ngSiege;
    }

    public void setNgSiege(int ngSiege) {
        this.ngSiege = ngSiege;
    }

    public Salle creer(){
        return new Salle();
    }
    public void modifier(){

    }

    public void afficher(){}


    @Override
    public String toString() {
        return "Salle{" +
                "id=" + id +
                ", ngSiege=" + ngSiege +
                '}';
    }
}
