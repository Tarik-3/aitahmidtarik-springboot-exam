package ma.emi.examen.entities;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Departement {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nom;
    @OneToMany(mappedBy = "departement",fetch = FetchType.EAGER)
    private List<Bureau> bureaus = new ArrayList<>();
    @OneToMany(mappedBy = "departement",fetch = FetchType.EAGER)
    private List<Prof> profs = new ArrayList<>();
    @ManyToOne
    @JoinColumn(nullable = false)
    private Universite universite;


    public Departement(String nom,Universite universite) {
        this.nom = nom;
        this.universite = universite;
    }
    public Departement(){}


    public Departement creer(){
        return new Departement();
    }
    public void modifier(){

    }

    public void afficher(){}
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Bureau> getBureaus() {
        return bureaus;
    }

    public void setBureaus(List<Bureau> bureaus) {
        this.bureaus = bureaus;
    }


    @Override
    public String toString() {
        return "Departement{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", bureaus=" + bureaus +
                ", profs=" + profs +
                '}';
    }
}
