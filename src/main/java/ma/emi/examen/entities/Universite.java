package ma.emi.examen.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;

import java.rmi.server.UID;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Entity

public class Universite {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String nom;



    @OneToMany(mappedBy = "universite")
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Map<Long,Personne> personneMap;
    @OneToMany(mappedBy = "universite",fetch = FetchType.EAGER)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<Salle> salles= new ArrayList<>();
    @OneToMany(mappedBy = "universite",fetch = FetchType.EAGER)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<Departement> departements = new ArrayList<>();
    @OneToMany(fetch = FetchType.EAGER)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<Bureau> bureaus = new ArrayList<>();



    private Universite(){}
    public Universite(String nom) {
        this.nom = nom;
    }

    public Universite creer(){
        return new Universite();
    }


    public void modifier(){

    }

    public void afficher(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Map<Long, Personne> getPersonneMap() {
        return personneMap;
    }

    public void setPersonneMap(Map<Long, Personne> personneMap) {
        this.personneMap = personneMap;
    }

    public List<Salle> getSalles() {
        return salles;
    }

    public void setSalles(List<Salle> salles) {
        this.salles = salles;
    }

    public List<Departement> getDepartements() {
        return departements;
    }

    public void setDepartements(List<Departement> departements) {
        this.departements = departements;
    }

    public List<Bureau> getBureaus() {
        return bureaus;
    }

    public void setBureaus(List<Bureau> bureaus) {
        this.bureaus = bureaus;
    }


    @Override
    public String toString() {
        return "Universite{" +
                "id=" + id +
                ", nom='" + nom + '\'' +
                ", personneMap=" + personneMap +
                ", salles=" + salles +
                ", departements=" + departements +
                ", bureaus=" + bureaus +
                '}';
    }
}
