package ma.emi.examen.entities;

import jakarta.persistence.Entity;

@Entity
public class Adjoint extends Prof{

    public Adjoint(String nom, Departement departement) {
        super(nom, departement);
    }

    public Adjoint() {
    }

    public Adjoint creer(){
        return new Adjoint();
    }
    public void modifier(){

    }

    public void afficher(){}
}
