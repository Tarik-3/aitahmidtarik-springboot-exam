package ma.emi.examen.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;

@Entity
public class Bureau {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany(mappedBy = "bureau",fetch = FetchType.EAGER)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<Employie> employies = new ArrayList<>();

//    @Override
//    public String toString() {
//        return "Bureau{" +
//                "id=" + id +
////                ", employies=" + employies +
//                ", departement=" + departement +
//                ", universite=" + universite +
//                '}';
//    }



    @ManyToOne
    @JoinColumn(nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Departement departement;

    @ManyToOne
    @JoinColumn(nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    Universite universite;

    public Bureau() {
    }


    public Bureau(Departement departement, Universite universite) {
        this.departement = departement;
        this.universite = universite;
    }

    public Bureau creer(){
        return new Bureau();
    }
    public void modifier(){

    }

    public void afficher(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Employie> getEmployies() {
        return employies;
    }

    public void setEmployies(List<Employie> employies) {
        this.employies = employies;
    }

    public Departement getDepartement() {
        return departement;
    }

    public void setDepartement(Departement departement) {
        this.departement = departement;
    }

    public Universite getUniversite() {
        return universite;
    }

    public void setUniversite(Universite universite) {
        this.universite = universite;
    }

    @Override
    public String toString() {
        return "Bureau{" +
                "id=" + id +
                ", employies=" + employies +
//                ", departement=" + departement +
//                ", universite=" + universite +
                '}';
    }
}
