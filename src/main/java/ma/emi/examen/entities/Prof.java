package ma.emi.examen.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;

@Entity
public class Prof extends Personne{
    @ManyToOne
    @JoinColumn(nullable = false)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Departement departement;



    public Prof(String nom, Departement departement) {
        super(nom);
        this.departement = departement;
    }

    public Prof() {
    }

    public Departement getDepartement() {
        return departement;
    }

    public void setDepartement(Departement departement) {
        this.departement = departement;
    }

    protected Prof creer(){
        return new Prof();
    }
    protected void modifier(){}
    protected void afficher(){}


    @Override
    public String toString() {
        return "Prof{" +
                "departement=" + departement +
                '}';
    }
}
