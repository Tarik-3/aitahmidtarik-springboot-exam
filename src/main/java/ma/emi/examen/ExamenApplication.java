package ma.emi.examen;


import ma.emi.examen.entities.*;

import ma.emi.examen.repositories.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ExamenApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenApplication.class, args);
	}

	@Bean
	CommandLineRunner kiki(
			PersonneRepository personneRepository,
			BureauRepository bureauRepository,
			DepartementRepository departementRepository,
			UniversiteRepository universiteRepository,
			SalleRepository salleRepository

	){
		return args -> {
			Universite universite = new Universite("EMI");

			universiteRepository.save(universite);


			Salle salle = new Salle(23,universite);
			Salle salle1 = new Salle(16,universite);
			Salle salle2 = new Salle(5,universite);
			Salle salle3 = new Salle(78,universite);

			salleRepository.save(salle);
			salleRepository.save(salle1);
			salleRepository.save(salle2);
			salleRepository.save(salle3);



			Departement Info = new Departement("Info",universite);
			Departement Electrique = new Departement("Electrique",universite);
			Departement Mecanique = new Departement("Mecanique",universite);

			departementRepository.save(Electrique);
			departementRepository.save(Info);
			departementRepository.save(Mecanique);


			Prof prof = new Associe("Tarik",Info);
			Prof prof1 = new Associe("Imad",Electrique);
			Prof prof2 = new Associe("Kabbaj",Info);
			Prof prof3 = new Associe("Mohmed",Electrique);

			personneRepository.save(prof);
			personneRepository.save(prof2);
			personneRepository.save(prof3);
			personneRepository.save(prof1);





			Bureau bureau = new Bureau(Info,universite);
			Bureau bureau2 = new Bureau(Info,universite);
			Bureau bureau3 = new Bureau(Mecanique,universite);
			Bureau bureau4 = new Bureau(Mecanique,universite);
			bureauRepository.save(bureau);
			bureauRepository.save(bureau2);
			bureauRepository.save(bureau3);
			bureauRepository.save(bureau4);


			Employie Hassan = new Employie("Hassan",bureau);
			Employie Toti = new Employie("Toti",bureau2);
			Employie Hakkou = new Employie("Hakkou",bureau);
			Employie Halima = new Employie("Halima",bureau3);
			personneRepository.save(Hassan);
			personneRepository.save(Toti);
			personneRepository.save(Halima);
			personneRepository.save(Hakkou);


//
//			System.out.println(prof.getNom());
//
//			personneRepository.findAll().forEach(p->{
//				System.out.println(p);
//			});
//			System.out.println("\tBureau*****Employer");
//
//			employie.setBureau(bureau);
//			personneRepository.save(employie);
//			personneRepository.findAll().forEach(p->{
//				System.out.println(p);
//			});
//
//
//			System.out.println("\tBureau*****Depar");
//			departementRepository.save(departement);
//			departementRepository.findAll().forEach(p->{
//				System.out.println(p);
//			});
//			bureauRepository.findAll().forEach(p->{
//				System.out.println(p);
//			});







		};
	}
}
