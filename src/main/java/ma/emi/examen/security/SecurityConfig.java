package ma.emi.examen.security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.withDefaults;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Bean
    public SecurityFilterChain coucou2(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(r->r
//                                .anyRequest().authenticated()
                        .requestMatchers(
                                "/personne/",
                                "/salle/",
                                "/bureau/",
                                "/universite/",
                                "/departement/"
                        ).permitAll()
                                .requestMatchers("/departement/get/{id}",
                                        "/personne/{{id}}",
                                        "/salle/get/{{id}}",
                                        "/universite/{{id}}",
                                        "/bureau/{{id}}"
                                        ).hasRole("USER")
                        .requestMatchers("bureau/delete/{id}",
                                "departement/delete/{id}",
                                "personne/delete/{id}",
                                "salle/delete/{id}",
                                "universite/delete/{id}"
                                ).hasRole("ADMIN")
//                        .requestMatchers("bureau/").permitAll()
                ).httpBasic(withDefaults());
        return http.build();

    }

    @Bean
    public UserDetailsService coucou(){
        UserDetails admin = User.withDefaultPasswordEncoder()
                .username("Tarik")
                .password("admin")
                .roles("ADMIN")
                .build();
        UserDetails user = User.withDefaultPasswordEncoder()
                .username("Kabbaj")
                .password("titi")
                .roles("USER")
                .build();

        return new InMemoryUserDetailsManager(admin,user);
    }



}
